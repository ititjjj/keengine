<?php

// +---------------------------------------------------------------------------+
// | This file is part of the core package.                                    |
// | Copyright (c) laiketui.com                                                |
// |                                                                           |
// | For the full copyright and license information, please view the LICENSE   |
// | file that was distributed with this source code. You can also view the    |
// | LICENSE file online at http://www.laiketui.com                            |
// +---------------------------------------------------------------------------+
declare (strict_types = 1);

namespace keengine\database;
use keengine\exception\DatabaseException;
use PDO;
use PDOException;

/**
 * PDODatabase provides connectivity for the PDO database abstraction layer.
 *
 * @package    keengine
 * @subpackage database
 *
 * @author ketter (ketter@laiketui.com)
 * @since  3.0.0
 */
class PDODatabase extends Database
{

    // +-----------------------------------------------------------------------+
    // | METHODS                                                               |
    // +-----------------------------------------------------------------------+

    /**
     * Connect to the database.
     *
     * @throws <b>DatabaseException</b> If a connection could not be created.
     *
     * @author ketter (ketter@laiketui.com)
     * @since  3.0.0
     */
    public function connect ()
    {

        // determine how to get our
        $method = $this->getParameter('method', 'normal');

        switch ($method)
        {

            case 'normal':

                // get parameters normally
                $database = $this->getParameter('database');
                $host     = $this->getParameter('host', '127.0.0.1');
                $password = $this->getParameter('password');
                $user     = $this->getParameter('user');
                $port     = $this->getParameter('port','3306');
                $charset  = $this->getParameter('charset','utf8');
                break;

            case 'server':

                // construct a connection string from existing $_SERVER values
                // and extract them to local scope
                $parameters =& $this->loadParameters($_SERVER);
                extract($parameters);

                break;

            case 'env':

                // construct a connection string from existing $_ENV values
                // and extract them to local scope
                $parameters =& $this->loadParameters($_ENV);
                extract($parameters);

                break;

            default:

                // who knows what the user wants...
                $error = 'Invalid MySQLDatabase parameter retrieval method ' .
                    '"%s"';
                $error = sprintf($error, $method);

                throw new DatabaseException($error);

        }

        try {
            $dbh = new PDO('mysql:dbname='.$database.';host='.$host.';port='.$port, $user, $password);
            $dbh->exec('set names '.$charset);
            $this->connection = $dbh;
        } catch (PDOException $e) {
            // the connection's foobar'd
            $error = 'Failed to create a MySQLDatabase connection';

            throw new DatabaseException($error);
        }

        // make sure the connection went through
        if ($this->connection === false)
        {

            // the connection's foobar'd
            $error = 'Failed to create a MySQLDatabase connection';

            throw new DatabaseException($error);

        }

        // since we're not an abstraction layer, we copy the connection
        // to the resource
        $this->resource =& $this->connection;

    }

    // -------------------------------------------------------------------------

    /**
     * Load connection parameters from an existing array.
     *
     * @return array An associative array of connection parameters.
     *
     * @author ketter (ketter@laiketui.com)
     * @since  3.0.0
     */
    private function & loadParameters (&$array)
    {

        // list of available parameters
        $available = array('database', 'host', 'password', 'user','port');

        $parameters = array();

        foreach ($available as $parameter)
        {

            $$parameter = $this->getParameter($parameter);

            $parameters[$parameter] = ($parameter != null)
                ? $array[$parameter] : null;

        }

        return $parameters;

    }

    // -------------------------------------------------------------------------

    /**
     * Execute the shutdown procedure.
     *
     * @return void
     *
     * @throws <b>DatabaseException</b> If an error occurs while shutting down
     *                                 this database.
     *
     * @author ketter (ketter@laiketui.com)
     * @since  3.0.0
     */
    public function shutdown ()
    {

        if ($this->connection != null)
        {

            $this->connection = null;

        }

    }

}

