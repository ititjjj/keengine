<?php

// +---------------------------------------------------------------------------+
// | This file is part of the core package.                                    |
// | Copyright (c) laiketui.com                                                |
// |                                                                           |
// | For the full copyright and license information, please view the LICENSE   |
// | file that was distributed with this source code. You can also view the    |
// | LICENSE file online at http://www.laiketui.com                            |
// +---------------------------------------------------------------------------+
declare (strict_types = 1);

namespace keengine;

define('KE_APP_NAME',          'LaiKeTui');
define('KE_APP_MAJOR_VERSION', '3');
define('KE_APP_MINOR_VERSION', '0');
define('KE_APP_MICRO_VERSION', '0');
define('KE_APP_BRANCH',        'dev-3.0.0');
define('KE_APP_STATUS',        'DEV');
define('KE_APP_VERSION',       KE_APP_MAJOR_VERSION . '.' .
                               KE_APP_MINOR_VERSION . '.' .
                               KE_APP_MICRO_VERSION . '-' . KE_APP_STATUS);
define('KE_APP_URL',           'http://www.laiketui.com');
define('KE_APP_INFO',          KE_APP_NAME . ' ' . KE_APP_VERSION .
                               ' (' . KE_APP_URL . ')');
define('KE_APP_DIR', dirname(__FILE__));

