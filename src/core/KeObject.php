<?php

// +---------------------------------------------------------------------------+
// | This file is part of the core package.                                    |
// | Copyright (c) laiketui.com                                                |
// |                                                                           |
// | For the full copyright and license information, please view the LICENSE   |
// | file that was distributed with this source code. You can also view the    |
// | LICENSE file online at http://www.laiketui.com                            |
// +---------------------------------------------------------------------------+
declare (strict_types = 1);

namespace keengine\core;
/**
 * $Id: KeObject.php 65 2004-10-26 03:16:15Z seank $
 *
 * KeObject provides useful methods that all Keengine classes inherit.
 *
 * @package    keengine
 * @subpackage core
 *
 * @author ketter (ketter@laiketui.com)
 * @since  3.0.0
 */
abstract class KeObject
{

    // +-----------------------------------------------------------------------+
    // | METHODS                                                               |
    // +-----------------------------------------------------------------------+

    /**
     * Retrieve a string representation of this object.
     *
     * @return string A string containing all public variables available in
     *                this object.
     *
     * @author ketter (ketter@laiketui.com)
     * @since  3.0.0
     */
    public function toString ()
    {

        $output = '';
        $vars   = get_object_vars($this);

        foreach ($vars as $key => &$value)
        {

            if (strlen($output) > 0)
            {

                $output .= ', ';

            }

            $output .= $key . ': ' . $value;

        }

        return $output;

    }

}

