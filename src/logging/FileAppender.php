<?php

// +---------------------------------------------------------------------------+
// | This file is part of the core package.                                    |
// | Copyright (c) laiketui.com                                                |
// |                                                                           |
// | For the full copyright and license information, please view the LICENSE   |
// | file that was distributed with this source code. You can also view the    |
// | LICENSE file online at http://www.laiketui.com                            |
// +---------------------------------------------------------------------------+
declare (strict_types = 1);

namespace keengine\logging;
/**
 *
 *
 * @package    keengine
 * @subpackage logging
 *
 * @author ketter (ketter@laiketui.com)
 * @since  3.0.0
 */
class FileAppender extends Appender
{

    // +-----------------------------------------------------------------------+
    // | CONSTANTS                                                             |
    // +-----------------------------------------------------------------------+

    // +-----------------------------------------------------------------------+
    // | PUBLIC VARIABLES                                                      |
    // +-----------------------------------------------------------------------+

    // +-----------------------------------------------------------------------+
    // | PRIVATE VARIABLES                                                     |
    // +-----------------------------------------------------------------------+

    // +-----------------------------------------------------------------------+
    // | CONSTRUCTOR                                                           |
    // +-----------------------------------------------------------------------+

    // +-----------------------------------------------------------------------+
    // | METHODS                                                               |
    // +-----------------------------------------------------------------------+

    function shutdown()
    {
        // TODO: Implement shutdown() method.
    }

    function write(&$data)
    {
        // TODO: Implement write() method.
    }
}

